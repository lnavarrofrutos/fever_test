package com.lnavarro.fevertest.presentation.ui;
/**
 * Created by luis on 10/01/18.
 */

import android.view.ViewGroup;

/**
 * MVP View that must be extended by all MVP views
 */
public interface BaseView {

    void setTitle(String title);

    ViewGroup getContainer();

}
