package com.lnavarro.fevertest.presentation.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lnavarro.fevertest.R;
import com.lnavarro.fevertest.domain.model.CrewMember;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmResults;

import static com.lnavarro.fevertest.domain.database.InitialDataRealmTransaction.AREAS;


public class CrewRecyclerAdapter extends RecyclerView.Adapter<CrewRecyclerAdapter.ViewHolder> {

    private RealmResults<CrewMember> mCrew;
    private Context mContext;

    public CrewRecyclerAdapter(Context context) {
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_crew, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final CrewMember crew = mCrew.get(position);

        if (crew.getArea().getName()
                .equalsIgnoreCase(AREAS.ENGINEERING.name())) {
            holder.mPositionText.setBackgroundColor(Color.RED);
        } else if (crew.getArea().getName()
                .equalsIgnoreCase(AREAS.COMMAND.name())) {
            holder.mPositionText.setBackgroundColor(Color.YELLOW);
        } else if (crew.getArea().getName()
                .equalsIgnoreCase(AREAS.SCIENCE.name())) {
            holder.mPositionText.setBackgroundColor(Color.BLUE);
        } else {
            holder.mPositionText.setBackgroundColor(Color.BLACK);

        }
        // Set crew name
        holder.mTextCrewName.setText(crew.getName());
        // Set crew race
        holder.mTextCrewRace.setText(crew.getRace().getName());

    }

    @Override
    public int getItemCount() {
        return mCrew.size();
    }


    public void setCrew(final RealmResults<CrewMember> crew) {
        mCrew = crew;
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.layout_item_container)
        LinearLayout mLayoutItem;
        @BindView(R.id.position_textview)
        TextView mPositionText;
        @BindView(R.id.crew_name)
        TextView mTextCrewName;
        @BindView(R.id.crew_race)
        TextView mTextCrewRace;

        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
