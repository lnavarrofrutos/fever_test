package com.lnavarro.fevertest.presentation.ui.home;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.lnavarro.fevertest.R;
import com.lnavarro.fevertest.domain.model.CrewMember;
import com.lnavarro.fevertest.presentation.ui.adapter.CrewRecyclerAdapter;
import com.lnavarro.fevertest.presentation.ui.BaseActivity;
import com.lnavarro.fevertest.presentation.ui.BasePresenter;
import com.lnavarro.fevertest.app.di.DiComponent;
import com.lnavarro.fevertest.app.di.components.ApplicationComponent;
import com.lnavarro.fevertest.app.di.components.CrewComponent;
import com.lnavarro.fevertest.app.di.components.DaggerCrewComponent;
import com.lnavarro.fevertest.app.di.modules.CrewModule;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.Unbinder;
import io.realm.RealmResults;

public class HomeActivity extends BaseActivity implements HomePresenter.HomeView {

    protected DiComponent component;

    Unbinder mUnbinder;

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @Inject
    HomePresenter presenter;

    private CrewRecyclerAdapter mAdapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initToolbar();
        initList();
    }

    @Override
    protected void closeRealm() {
        presenter.closeRealm();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }

    @Override
    protected void setupComponent(ApplicationComponent appComponent) {
        component = DaggerCrewComponent.builder()
                .crewModule(new CrewModule(this))
                .applicationComponent(appComponent)
                .build();
        ((CrewComponent) component).inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_home;
    }

    @Override
    protected BasePresenter getPresenter() {
        return null;
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
    }

    private void initList() {
        mAdapter = new CrewRecyclerAdapter(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);

        presenter.showCrewByName();
    }

    @Override
    public void showCrew(RealmResults<CrewMember> allCrew) {
        if(mAdapter != null){
            mAdapter.setCrew(allCrew);
        }
    }
}
