package com.lnavarro.fevertest.presentation.ui.home;

import com.lnavarro.fevertest.domain.database.DatabaseService;
import com.lnavarro.fevertest.domain.model.CrewMember;
import com.lnavarro.fevertest.presentation.ui.BasePresenter;

import io.realm.RealmResults;

/**
 * Created by luis on 10/01/18.
 */

public class HomePresenter implements BasePresenter {

    private final DatabaseService mRealmService;
    private HomeView mView;

    private boolean crewShown = false;

    public HomePresenter(HomeView crewView, DatabaseService mRealmService) {
        this.mView = crewView;
        this.mRealmService = mRealmService;
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onBackPressed() {

    }

    public void closeRealm() {
        mRealmService.closeRealm();
    }

    public void showCrewByName() {
        if (!crewShown) {
            mView.showCrew(mRealmService.getMemmersCrew());
            crewShown = true;
        }
    }

    public interface HomeView {
        public void showCrew(RealmResults<CrewMember> allCrew);
    }

}
