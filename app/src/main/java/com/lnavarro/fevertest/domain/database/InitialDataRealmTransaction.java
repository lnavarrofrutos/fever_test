package com.lnavarro.fevertest.domain.database;

import com.lnavarro.fevertest.utils.Utils;
import com.lnavarro.fevertest.domain.model.Race;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by luis on 10/01/18.
 */

public class InitialDataRealmTransaction implements Realm.Transaction {

    private static final int MAX_CREW_MEMBERS = 500;

    public enum RACE {
        HUMAN, BETAZOID, VULCAN;

        public static RACE getRandom() {
            return values()[(int) (Math.random() * values().length)];
        }
    }

    public enum COLOR {
        BLUE, RED, YELLOW, OTHER;
    }

    public enum AREAS {
        SCIENCE, ENGINEERING, COMMAND;

        public static AREAS getRandom() {
            return values()[(int) (Math.random() * values().length)];
        }

        public static COLOR getColor(String areas) {
            if (areas.equalsIgnoreCase(SCIENCE.name())) {
                return COLOR.BLUE;
            } else if (areas.equalsIgnoreCase(ENGINEERING.name())) {
                return COLOR.RED;
            } else if (areas.equalsIgnoreCase(COMMAND.name())) {
                return COLOR.YELLOW;
            } else {
                return COLOR.OTHER;
            }
        }
    }

    DatabaseService service;

    @Override
    public void execute(Realm realm) {
        service = new DatabaseService(realm);

        // Init Races
        service.addRace(RACE.HUMAN.name());
        service.addRace(RACE.VULCAN.name());
        service.addRace(RACE.BETAZOID.name());
        RealmResults<Race> allRaces = service.getAllRaces();


        // Init Areas
        service.addArea(AREAS.SCIENCE.name());
        service.addArea(AREAS.COMMAND.name());
        service.addArea(AREAS.ENGINEERING.name());

        // Loop to init members crew
        for (int i = 0; i < MAX_CREW_MEMBERS; i++) {
            service.addCrew(Utils.generateRandomName(6), AREAS.getRandom().name(),
                    RACE.getRandom().name());
        }
    }
}
