package com.lnavarro.fevertest.domain.database;


import com.lnavarro.fevertest.domain.model.Area;
import com.lnavarro.fevertest.domain.model.CrewMember;
import com.lnavarro.fevertest.domain.model.Race;

import io.realm.Realm;
import io.realm.RealmResults;

public class DatabaseService {

    private static final String ID = "id";
    private static final String NAME = "name";

    private final Realm mRealm;

    public DatabaseService(final Realm realm) {
        mRealm = realm;
    }

    public void closeRealm() {
        mRealm.close();
    }

    public RealmResults<CrewMember> getMemmersCrew() {
        return mRealm.where(CrewMember.class).findAll().sort(NAME);
    }

    public RealmResults<Race> getAllRaces() {
    return mRealm.where(Race.class).findAll();
}

    public void addCrew(final String name, final String area, final String race) {
        CrewMember crew = mRealm.createObject(CrewMember.class, mRealm.where(CrewMember.class).findAll().size());
        crew.setName(name);
        crew.setArea(createOrGetArea(area, mRealm));
        crew.setRace(createOrGetRace(race, mRealm));
        mRealm.insertOrUpdate(crew);
    }

    public void addRace(final String name) {
        Race race = mRealm.createObject(Race.class, mRealm.where(Race.class).findAll().size());
        race.setName(name);
        mRealm.insertOrUpdate(race);
    }

    public void addArea(final String name) {
        Area area = mRealm.createObject(Area.class, mRealm.where(Area.class).findAll().size());
        area.setName(name);
        mRealm.insertOrUpdate(area);
    }

    private Area createOrGetArea(final String name, final Realm realm) {
            Area area = realm.where(Area.class).equalTo(NAME, name).findFirst();
            if (area == null) {
                area = addArea(name, realm);
            }
            return area;
    }

    private Race createOrGetRace(final String name, final Realm realm) {
            Race race =  realm.where(Race.class).equalTo(NAME, name).findFirst();
            if (race == null) {
                race = addRace(name, realm);
            }
            return race;
    }

    private Area addArea(final String name, final Realm realm) {
            Area area = realm.createObject(Area.class);
            area.setId(realm.where(Area.class).findAll().size());
            area.setName(name);
            return area;
    }

    private Race addRace(final String name, final Realm realm) {
            Race race = realm.createObject(Race.class);
            race.setId(realm.where(Race.class).findAll().size());
            race.setName(name);
            return race;
    }

}
