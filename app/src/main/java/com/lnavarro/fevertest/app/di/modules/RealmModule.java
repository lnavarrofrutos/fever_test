package com.lnavarro.fevertest.app.di.modules;

import com.lnavarro.fevertest.domain.database.DatabaseService;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;

/**
 * Created by luis on 10/01/18.
 */

@Module
public class RealmModule {

    @Provides
    Realm provideRealm() {
        return Realm.getDefaultInstance();
    }

    @Provides
    DatabaseService provideRealmService(final Realm realm) {
        return new DatabaseService(realm);
    }
}
