package com.lnavarro.fevertest.app;

import android.app.Application;
import android.content.Context;

import com.lnavarro.fevertest.domain.database.InitialDataRealmTransaction;
import com.lnavarro.fevertest.app.di.components.ApplicationComponent;
import com.lnavarro.fevertest.app.di.components.DaggerApplicationComponent;
import com.lnavarro.fevertest.app.di.modules.ApplicationModule;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by luis on 10/01/18.
 */

public class App extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        setupInjector();
        initDatabase();
    }

    private void setupInjector() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        applicationComponent.inject(this);
    }

    private void initDatabase() {
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .initialData(new InitialDataRealmTransaction())
                .build();
        Realm.setDefaultConfiguration(config);
    }

    public static App getApp(Context context) {
        return (App) context.getApplicationContext();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

}
