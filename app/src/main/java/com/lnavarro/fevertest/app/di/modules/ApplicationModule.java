package com.lnavarro.fevertest.app.di.modules;

import android.content.Context;
import com.lnavarro.fevertest.app.App;

import dagger.Module;
import dagger.Provides;

/**
 * Created by luis on 10/01/18.
 */

@Module
public class ApplicationModule {

    private final App application;

    public ApplicationModule(App application) {
        this.application = application;
    }

    @Provides
    public App provideApplication() {
        return application;
    }

    @Provides
    public Context provideContext() {
        return application;
    }

}
