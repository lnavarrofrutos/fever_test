package com.lnavarro.fevertest.app.di.components;

import com.lnavarro.fevertest.presentation.ui.home.HomeActivity;
import com.lnavarro.fevertest.app.di.DiComponent;
import com.lnavarro.fevertest.app.di.modules.CrewModule;

import dagger.Component;

/**
 * Created by luis on 10/01/18.
 */

@Component(modules = {CrewModule.class,}, dependencies = {ApplicationComponent.class})

public interface CrewComponent extends DiComponent {
    void inject(HomeActivity loginActivity);
}