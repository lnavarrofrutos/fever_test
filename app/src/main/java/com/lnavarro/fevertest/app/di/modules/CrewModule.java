package com.lnavarro.fevertest.app.di.modules;

import com.lnavarro.fevertest.domain.database.DatabaseService;
import com.lnavarro.fevertest.presentation.ui.home.HomeActivity;
import com.lnavarro.fevertest.presentation.ui.home.HomePresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by luis on 10/01/18.
 */

@Module
public class CrewModule {
    private final HomeActivity mActivity;

    public CrewModule(HomeActivity mActivity) {
        this.mActivity = mActivity;
    }

    @Provides
    public HomePresenter.HomeView providesCrewActivity(){
        return this.mActivity;
    }

    @Provides
    public HomePresenter providesCrewPresenter(DatabaseService realmService) {
        return new HomePresenter(this.mActivity, realmService);
    }
}
