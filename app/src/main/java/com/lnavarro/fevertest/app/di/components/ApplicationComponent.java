package com.lnavarro.fevertest.app.di.components;

import android.content.Context;

import com.lnavarro.fevertest.app.App;
import com.lnavarro.fevertest.domain.database.DatabaseService;
import com.lnavarro.fevertest.app.di.modules.ApplicationModule;
import com.lnavarro.fevertest.app.di.modules.RealmModule;

import dagger.Component;
import io.realm.Realm;

/**
 * Created by luis on 10/01/18.
 */

@Component(modules = {ApplicationModule.class, RealmModule.class})
public interface ApplicationComponent {

    void inject(App application);

    Context getContext();

    Realm provideRealm();

    DatabaseService provideRealmService();

}
